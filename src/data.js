import { ref } from "vue"
import pdp from './assets/avatara/profile0.jpg';
import pdp1 from './assets/avatara/profile1.jpg';
import pdp2 from './assets/avatara/profile2.jpg';
import call from './assets/Illustration/call-to-action.png'
import call2 from './assets/Illustration/feature-bg.png'
import call1 from './assets/Illustration/cta.png'
import serasera from './assets/Img/serasera1.jpg';
import eatiala from './assets/Img/photo.jpg';
import iste from './assets/Img/mail_box.png';
import etute from './assets/Img/etude.jpg'
import ambozotany from './assets/IMAGEFIANAR/AMBOZONTANY.jpg';
import ANJOMA from './assets/IMAGEFIANAR/ANJOMA.jpg';
import CATHEDRALE from './assets/IMAGEFIANAR/CATHEDRALE.jpg';
import MASINAMARIA from './assets/IMAGEFIANAR/MASINAMARIA.jpg';
import LAPANTANANA from './assets/IMAGEFIANAR/LAPANTANANA.jpg';
import GARRE from './assets/IMAGEFIANAR/GARRE.jpg';
import STADE from './assets/IMAGEFIANAR/STADE.jpg';

export const conseils = ref([
  { idConseil: 'azoeeoi1', contenue: 'Participez à des événements locaux et explorez de nouvelles activités.', date: '25-03-2025', idUser: 'zzzz', statue: true, idKey: 'zzzz' },
  { idConseil: 'azoeeoi2', contenue: 'Acceptez que les premiers jours puissent être difficiles, mais restez optimiste.', date: '25-03-2025', idUser: 'zzzz', statue: true, idKey: '' },
  { idConseil: 'azoeeoi3', contenue: 'Découvrez la ville ou la région où se trouve l\'université', date: '25-03-2025', idUser: 'ttz', statue: true, idKey: '' },
  { idConseil: 'azoeeoi4', contenue: 'Soyez ouvert aux opportunités qui se présentent.', date: '25-03-2025', idUser: 'bbb', statue: true, idKey: '' },
  { idConseil: 'azoeeoi5', contenue: 'Soyez ouvert aux opportunités qui se présentent.', date: '25-03-2025', idUser: 'zzzz', statue: true, idKey: '' },
  { idConseil: 'azoeeoi6', contenue: 'Soyez prêt à vous adapter à un nouvel environnement et à de nouvelles méthodes d\'apprentissage.', date: '25-03-2025', idUser: 'bbb', statue: true, idKey: '' },
  { idConseil: 'azoeeoi7', contenue: 'Assurez-vous d\'assister à tous vos cours.', date: '25-03-2025', idUser: 'zzzz', statue: true, idKey: '' },
  { idConseil: 'azoeeoi8', contenue: 'N\'hésitez pas à poser des questions aux enseignants si vous avez des doutes.', date: '25-03-2025', idUser: 'ttz', statue: true, idKey: '' },
])
export const user = ref([
  { idUser: "zzzz", nom: "mika", prenom: 'NANDRASANA', password: 'mika', email: 'mika@gmail.com', photo: pdp1, statue: '', adresse: 'Fianarantsoa', numero: '0326454167' },
  { idUser: "bbb", nom: "Rakoto", prenom: 'RANDRIANASOLO', password: 'mika', email: 'mika@gmail.com', photo: pdp, statue: '', adresse: 'Fianarantsoa', numero: '0326454167' },
  { idUser: "ttz", nom: "Randria", prenom: 'FANIRY', password: 'mika', email: 'mika@gmail.com', photo: pdp2, statue: '', adresse: 'Fianarantsoa', numero: '0326454167' },
  { idUser: "uuuzz", nom: "Meva", prenom: 'RAKOTO', password: 'mika', email: 'mika@gmail.com', photo: pdp, statue: '', adresse: 'Fianarantsoa', numero: '0326454167' },
  { idUser: "pppz", nom: "Ranto", prenom: 'RASANA', password: 'mika', email: 'mika@gmail.com', photo: pdp2, statue: '', adresse: 'Fianarantsoa', numero: '0326454167' },
])
export const evenets = ref([
  { idEvenement: 'ooto', date: '09-05-2024', photo: call, contenue: "Reception des novices de l'ENI", idkey: 'iii' },
  { idEvenement: 'oyoo', date: '09-02-2024', photo: call1, contenue: "Conference d'entreprise ", idkey: 'ttt' },
  { idEvenement: 'oppo', date: '03-03-2024', photo: call2, contenue: "Deposition de CV ", idkey: 'ttt' },
  { idEvenement: 'oomo', date: '09-04-2024', photo: call, contenue: " Reception des novices du faculté DEGS", idkey: 'zzzz' },
  { idEvenement: 'ooop', date: '10-09-2024', photo: call1, contenue: "Assemblé générale ", idkey: 'ooo' },
  { idEvenement: 'ooto', date: '23-05-2024', photo: call1, contenue: "Conference d'entreprise ", idkey: 'iii' },
  { idEvenement: 'ooho', date: '09-05-2024', photo: call2, contenue: " Formation avec Orange digital center", idkey: 'zzzz' },
])
export const keys = ref([
  { idkey: 'iii', contenue: 'Informatique' },
  { idkey: 'zzzz', contenue: 'DEGSS' },
  { idkey: 'ttt', contenue: 'Droit' },
  { idkey: 'uuu', contenue: 'Economie' },
  { idkey: 'ooo', contenue: 'Gestion' },
  { idkey: 'ppp', contenue: 'Informatique' },
  { idkey: 'ppp', contenue: 'Informatique' },
])
export const posts = ref([
  { idPost: 'oooooo', date: '02-04-2020', contenue: 'Comment bien le cours à l\'université ??', idUser: 'zzzz' },
  { idPost: 'oooooo1', date: '02-06-2022', contenue: 'La Vie universitaire est comment ??', idUser: 'bbb' },
  { idPost: 'oooooo2', date: '02-10-2023', contenue: 'La Vie universitaire est comment  à l\ENI??', idUser: 'ttz' },
  { idPost: 'oooooo3', date: '06-10-2024', contenue: 'Comment mémorisé une livre de 500 pages ??', idUser: 'zzzz' },
])
export const etablissement = ref([
  { idEtablissement : "elllll" , nom : 'Serasera', type : 'Sa', adresse : 'Andrainjato', description : `Le numérique a la porter de tous`, photo : eatiala},
  { idEtablissement : "elllll" , nom : 'e-atiala', type : 'Sa', adresse : 'Andrainjato', description : `Etudié en groupe`, photo : serasera},
  { idEtablissement : "elllll" , nom : 'Rova', type : 'Sa', adresse : 'Ambozotany', description : `Lieu culturel`, photo : ambozotany},
  { idEtablissement : "elllll" , nom : 'CDI', type : 'Sa', adresse : 'Tsianolondroa', description : `Affichage sur le recherche des emploié`, photo : iste},
  { idEtablissement : "elllll" , nom : 'Masin\'i Maria', type : 'Sa', adresse : 'Tanana ambony', description : `Moniment`, photo : MASINAMARIA},
  { idEtablissement : "elllll" , nom : 'Lapan\'ny tanana', type : 'Sa', adresse : 'Anjoma', description : `la commune`, photo : LAPANTANANA},
  { idEtablissement : "elllll" , nom : 'Cathedrale', type : 'Sa', adresse : 'AMBOZITANY', description : `Eglise catolique`, photo : CATHEDRALE},
  { idEtablissement : "elllll" , nom : 'Garre', type : 'Sa', adresse : 'Ampasambazaha', description : `La darre du train FCE`, photo : GARRE},
  { idEtablissement : "elllll" , nom : 'Stade', type : 'Sa', adresse : 'Andrainjato', description : `Stade minicipale`, photo : STADE},
  
])
export const commentaires = ref([
  { idcommentaires: 'azoeeoi', contenue: 'créer une agenda', date: '25-03-2024', idUser: 'zzzz', idPost: 'oooooo' },
  { idcommentaires: 'azoeeoi', contenue: 'faire de recherche ', date: '25-03-2024', idUser: 'zzzz', idPost: 'oooooo' },
  { idcommentaires: 'azoeeoi', contenue: 'bien pratique', date: '25-03-2024', idUser: 'zzzz', idPost: 'oooooo2' },
  { idcommentaires: 'azoeeoi', contenue: 'La réussite dans les études dépend de plusieurs facteurs, dont la motivation, l\'organisation et les habitudes d\'apprentissage. Voici quelques conseils généraux pour vous aider à exceller dans vos études :', date: '25-03-2025', idUser: 'bbb',  idPost: 'oooooo3' },
  { idcommentaires: 'azoeeoi', contenue: 'lIMITER LE TEMPS DE DIVERSTISSEMENT', date: '25-04-2024', idUser: 'zzzz',  idPost: 'oooooo' },
])
