import { user as allUser, keys } from './data'
import { ref } from 'vue'
const noUser = { idUser: '', nom: '', prenom: '', statue: '', photo: '', email: '', adresse: '', numero: '', password: '' }
const noKey = { idkey: '', contenue: '' }
export const log = ref(true)
export const getKeyEvent = (idKey) => {
  let currentKey = keys.value.find(keys => keys.idkey === idKey)
  if (currentKey) {
    return currentKey;
  }
  else return noKey
}
export const getUser = (idUser) => {
  let currentUser = allUser.value.find(user => user.idUser === idUser)
  if (currentUser) {
    return currentUser;
  }
  else return noUser
}