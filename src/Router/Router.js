import { createRouter, createWebHistory } from 'vue-router';
import Event from '../components/Event/Event.vue';
import Home from '../components/Home/Home.vue';
import Forome from '../components/Forome/Forome.vue';
import Conseil from '../components/Conseil/Conseil.vue';
import Signup from '../components/SIngup/Singup.vue'
import Login from '../components/Login/Login.vue';
import DescriptionCity from '../components/DescriptionCity/DescriptionCity.vue';
import Comment from '../components/Forome/Comment/Comment.vue';
export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/conseil',
      name: 'conseil',
      component: Conseil,
    },
    {
      path: '/forome',
      name: 'frorome',
      component: Forome,
    },
    {
      path: '/event',
      name: 'event',
      component: Event,
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/descriptionCity',
      name: 'descriptionCity',
      component: DescriptionCity
    },
    {
      path: '/comment/:idPost',
      name: 'comment',
      component: Comment
    },
  ]
})