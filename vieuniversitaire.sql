-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 07 mars 2024 à 04:59
-- Version du serveur : 8.0.31
-- Version de PHP : 8.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `vieuniversitaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `idCommentaire` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `contenue` varchar(200) DEFAULT NULL,
  `idPost` varchar(255) DEFAULT NULL,
  `idUser` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `conseil`
--

DROP TABLE IF EXISTS `conseil`;
CREATE TABLE IF NOT EXISTS `conseil` (
  `idConseil` varchar(255) DEFAULT NULL,
  `contenue` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `idkey` varchar(100) DEFAULT NULL,
  `statue` tinyint(1) DEFAULT NULL,
  `idUser` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement`
--

DROP TABLE IF EXISTS `etablissement`;
CREATE TABLE IF NOT EXISTS `etablissement` (
  `idEtablissement` varchar(255) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `adresse` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `photo` longblob
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `etablissement`
--

INSERT INTO `etablissement` (`idEtablissement`, `nom`, `type`, `adresse`, `description`, `photo`) VALUES
('', '', '', '', '','');

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
CREATE TABLE IF NOT EXISTS `evenement` (
  `idEvenement` varchar(255) DEFAULT NULL,
  `photo` longblob,
  `contenue` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `idkey` varchar(100) DEFAULT NULL,
  `idUser` varchar(255) DEFAULT NULL,
  `titre` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `evenement`
--

INSERT INTO `evenement` (`idEvenement`, `photo`, `contenue`, `date`, `idkey`, `idUser`, `titre`) VALUES
('1', NULL, 'hhh', '0000-00-00', 'hhh', '', ''),
('1', NULL, 'hhh', '0000-00-00', 'hhh', 'kk', 'hhh');
INSERT INTO `evenement` (`idEvenement`, `photo`, `contenue`, `date`, `idkey`, `idUser`, `titre`) VALUES
('','', '', '0000-00-00', '', '', NULL);
INSERT INTO `evenement` (`idEvenement`, `photo`, `contenue`, `date`, `idkey`, `idUser`, `titre`) VALUES
('', '', '', '2024-03-05', '', 'undefined', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `keytable`
--

DROP TABLE IF EXISTS `keytable`;
CREATE TABLE IF NOT EXISTS `keytable` (
  `idkey` varchar(100) DEFAULT NULL,
  `contenue` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `keytable`
--

INSERT INTO `keytable` (`idkey`, `contenue`) VALUES
(NULL, 'FDS'),
('', ''),
('', 'hhh'),
('2', 'hhh'),
('1', 'Informatique');

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `idPost` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `contenue` varchar(200) DEFAULT NULL,
  `idkey` varchar(100) DEFAULT NULL,
  `idUser` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` varchar(255) DEFAULT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(100) DEFAULT NULL,
  `statue` varchar(50) DEFAULT NULL,
  `photo` longblob,
  `email` varchar(200) DEFAULT NULL,
  `adresse` varchar(100) DEFAULT NULL,
  `numero` varchar(11) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`idUser`, `nom`, `prenom`, `statue`, `photo`, `email`, `adresse`, `numero`, `password`) VALUES
('1', 'sylvio', '', '', NULL, '', '', '', '');
INSERT INTO `user` (`idUser`, `nom`, `prenom`, `statue`, `photo`, `email`, `adresse`, `numero`, `password`) VALUES
('67f4128f-1d5e-49ad-8765-34585bcd1fda', 'ff', 'ff', '0', '', 'dd', 'dd', '444', 'dd');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
